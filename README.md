Este é um trabalho de tradução do jogo Divinity Original Sin para a lingua Português - Brasil.

O arquivo está atualmente traduzido, porém com problemas de lógica, erros de escrita e palavras em outras linguas. 

Nosso trabalho é o de corrigir os problemas acima citados, por meio da revisão do texto com base no arquivo original em inglês, arquivo traduzido e mediante jogabilidade.

Os arquivos estarão sincronizados em tempo real conforme as correções nos textos, ou seja, a qualquer momento você pode pegar o arquivo mais atualizado, ora ainda não concluido.

Para contribuir com a revisão mande um e-mail para fernandobuzzy@hotmail.com.

Antes de realizar o comando PULL, você precisa fazer o COMMIT (sem o push) dos arquivos que você alterou, caso contrário, as suas alterações não vão ser aceitas.
É importante que os arquivos no servidor estejam LIMPOS, ou seja, sem erros e de preferência compilando (facilite a programação do seu amiguinho)
Caso haja conflitos, o GIT vai indicar a classe e a linha onde ocorreu o conflito, que vai ser interpretado por VOCÊ, ou seja, esteja a par do que está acontecendo.
Comandos:

Para subir arquivos no servidor:

git status //mostra todas as alterações feitas por você, no código atual do seu PC
git add . // cria um pacote com as suas alterações 
git commit -m "comentário" //realiza o commit, porém apenas no seu computador, ainda não foi para o servidor
git push //envia os arquivos para o servidor.

Para pegar os arquivos do servidor:

git pull //puxa os arquivos do servidor para o seu computador (É aqui que ocorrerão os conflitos, caso duas pessoas estejam desenvolvendo o mesmo código)